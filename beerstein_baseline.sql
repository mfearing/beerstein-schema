insert into beerstein.user (name, pass)
values ("bsmaster", "12345");

insert into beerstein.category (name, type)
values ("Stout", "beer"),
("Lager", "beer"),
("Grain", "ingredient"),
("Yeast", "ingredient"),
("Hops", "ingredient"),
("Misc", "ingredient");


insert into beerstein.recipe (user_id, category_id, name)
values (1, 1, "Coffee Stout"),
(1, 2, "Light Lagerdale");


insert into beerstein.ingredient (recipe_id, category_id, name, quantity, measurement) values (1, 3, "Wheat", 9.5, "lbs");
insert into beerstein.ingredient (recipe_id, category_id, name, quantity, measurement) values (1, 3, "Malt", 1.5, "lbs");
insert into beerstein.ingredient (recipe_id, category_id, name, quantity, measurement) values (1, 4, "Yeast", 1.0, "packet");
insert into beerstein.ingredient (recipe_id, category_id, name, quantity, measurement) values (1, 5, "Hops", 2, "oz");
insert into beerstein.ingredient (recipe_id, category_id, name, quantity, measurement) values (1, 6, "Sugar", 1.5, "cups");

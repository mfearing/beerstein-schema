create database beerstein;

use beerstein;

create table user
(
	ID int NOT NULL auto_increment, 
    NAME VARCHAR(60) NOT NULL,
    PASS VARCHAR(20) NOT NULL,
    primary key (ID)
);

create table category
(
	ID int NOT NULL auto_increment,
    NAME varchar(60) NOT NULL,
	TYPE varchar(20) NOT NULL,
    primary key(ID)
);

create table recipe
(
	ID int NOT NULL auto_increment,
    NAME varchar(60) NOT NULL, 
    USER_ID int NOT NULL,
    CATEGORY_ID int NOT NULL,
    DIRECTIONS TEXT,
    primary key (ID),
    FOREIGN KEY (USER_ID) REFERENCES user(ID) ON DELETE CASCADE,
    FOREIGN KEY (CATEGORY_ID) REFERENCES category(ID)
);

create table ingredient
(
	ID int not null auto_increment,
    NAME varchar(60) not null,
    RECIPE_ID int not null,
    CATEGORY_ID int NOT NULL,
    QUANTITY DOUBLE NOT NULL,
    MEASUREMENT varchar(60),
    primary key(ID),
    FOREIGN KEY (RECIPE_ID) references recipe(ID),
    FOREIGN KEY (CATEGORY_ID) REFERENCES category(ID)
);

